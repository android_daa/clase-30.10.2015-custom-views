package com.example.duoc.customviews;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.duoc.customviews.views.PerfilCustomView;


public class MainActivity extends ActionBarActivity {

    private PerfilCustomView customGallina;
    private PerfilCustomView customSuperman;
    private PerfilCustomView customBatman;
    private PerfilCustomView customTerminator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        customGallina = (PerfilCustomView)findViewById(R.id.customGallina);
        customSuperman = (PerfilCustomView)findViewById(R.id.customSuperman);
        customSuperman.setOnClickPerfilListener(new PerfilCustomView.OnClickPerfilListener() {
            @Override
            public void onClickOK(String titulo, String descripcion) {
                Toast.makeText(getApplicationContext(), "OK-- Titulo: " + titulo + "\n Descripcion: " + descripcion, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onClickCancel(String titulo, String descripcion) {
                Toast.makeText(getApplicationContext(), "CANCEL -- Titulo: " + titulo + "\n Descripcion: " + descripcion, Toast.LENGTH_LONG).show();
            }
        });

    }


}
