package com.example.duoc.customviews.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.duoc.customviews.R;

/**
 * Created by Duoc on 30-10-2015.
 */
public class PerfilCustomView extends LinearLayout {

    private ImageView perfil;
    private TextViewCustom txtTitulo;
    private TextViewCustom txtDescripcion;
    private Button btnOK;
    private Button btnCancel;
    private OnClickPerfilListener listener;

    public PerfilCustomView(Context context) {
        super(context);
        innflateCustom();
    }

    public PerfilCustomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        innflateCustom();
        init(attrs);
    }

    public PerfilCustomView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        innflateCustom();
        init(attrs);
    }

    private void innflateCustom() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout linearLayoutItem = (LinearLayout) inflater.inflate(R.layout.view_compuesto_item, this);
        perfil = (ImageView)linearLayoutItem.findViewById(R.id.perfil);
        txtTitulo = (TextViewCustom)linearLayoutItem.findViewById(R.id.txtTitulo);
        txtDescripcion= (TextViewCustom)linearLayoutItem.findViewById(R.id.txtDescripcion);
        btnOK = (Button)linearLayoutItem.findViewById(R.id.btnOK);
        btnCancel = (Button)linearLayoutItem.findViewById(R.id.btnCamcel);
    }

    private void init(AttributeSet attrs){
        TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.PerfilCustomView, 0, 0);
        try{
            int tipoPerfil = a.getInteger(R.styleable.PerfilCustomView_tipo_perfil, 0);
            cargarTipoPerfil(tipoPerfil);
            String titulo = a.getString(R.styleable.PerfilCustomView_titulo);
            txtTitulo.setText(titulo);
            String descripcion = a.getString(R.styleable.PerfilCustomView_descripcion);
            txtDescripcion.setText(descripcion);
            boolean muestraBotones = a.getBoolean(R.styleable.PerfilCustomView_muestra_eventos,false);
            if(muestraBotones == false){
                btnOK.setVisibility(View.GONE);
                btnCancel.setVisibility(View.GONE);
            }else{
                btnOK.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(listener != null){
                            listener.onClickOK(txtTitulo.getText().toString(), txtDescripcion.getText().toString());
                        }
                    }
                });

                btnCancel.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(listener != null){
                            listener.onClickCancel(txtTitulo.getText().toString(), txtDescripcion.getText().toString());
                        }
                    }
                });
            }


        }finally {
            a.recycle();
        }
    }

    private void cargarTipoPerfil(int param){
        int imagen = R.drawable.batman;
        if(param == 0){
            imagen = R.drawable.batman;
        }else if(param == 1){
            imagen = R.drawable.gallina;
        }else if(param == 2){
            imagen = R.drawable.superman;
        }else if(param == 3){
            imagen = R.drawable.terminator;
        }
        perfil.setImageResource(imagen);
    }

    public void setOnClickPerfilListener(OnClickPerfilListener listener){
        this.listener = listener;
    }

    public interface OnClickPerfilListener{
        void onClickOK(String titulo, String descripcion);
        void onClickCancel(String titulo, String descripcion);
    }
}
