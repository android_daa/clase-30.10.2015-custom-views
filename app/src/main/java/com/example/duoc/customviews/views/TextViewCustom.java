package com.example.duoc.customviews.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;
import android.widget.Toast;

import com.example.duoc.customviews.R;

/**
 * Created by Duoc on 30-10-2015.
 */
public class TextViewCustom extends TextView {

    private int tipoFuente = -1;

    public TextViewCustom(Context context, TIPO_FUENTE tipoFuente) {
        super(context);
        setTipoFuente(tipoFuente);
        //setFuente(-1);
    }

    public TextViewCustom(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public TextViewCustom(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs){
        TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.TextFieldCustom, 0, 0);
        try{
            tipoFuente = a.getInteger(R.styleable.TextFieldCustom_tipo_fuente, -1);
            boolean imprime = a.getBoolean(R.styleable.TextFieldCustom_imprime, false);

            if(imprime == true){
                Toast.makeText(getContext(), "Seleccionaste True", Toast.LENGTH_LONG).show();
            }

        }finally {
            a.recycle();
        }
        setFuente(tipoFuente);
    }

    private void setTipoFuente(TIPO_FUENTE tipoFuente){
        if(tipoFuente == TIPO_FUENTE.DEFAULT){
            setFuente(-1);
        }else if(tipoFuente == TIPO_FUENTE.HELLO_SCRIPT){
            setFuente(0);
        }else if(tipoFuente == TIPO_FUENTE.MISTUKI){
            setFuente(1);
        }else if(tipoFuente == TIPO_FUENTE.NEXT_CUSTOM){
            setFuente(2);
        }
    }

    private void setFuente(int tipoFuente){
        try{
            String fuente = "";
            if(tipoFuente==0){
                fuente = "HelloScript.ttf";
            }else if(tipoFuente==1){
                fuente = "Mistuki.ttf";
            }else if(tipoFuente == 2){
                fuente = "Next Custom.ttf";
            }


            Typeface font = Typeface.createFromAsset(
                    getContext().getAssets(),
                    fuente);

            this.setTypeface(font);

        }catch (Exception e){

        }
    }


    public enum TIPO_FUENTE{
        HELLO_SCRIPT,
        MISTUKI,
        NEXT_CUSTOM,
        DEFAULT
    }
}
